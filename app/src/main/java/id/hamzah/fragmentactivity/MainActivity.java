package id.hamzah.fragmentactivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    //Inisialisasi Pertama di Java
    Button btnprofil1;
    Button btnmycity1;
    Button btnmyeducation1;
    Button btnmyfamily1;
    Button btnquiz1;
    Button btnquit1;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inisialisasi Kedua di Java
        btnprofil1 = (Button) findViewById(R.id.btnprofil);
        btnmycity1 = (Button) findViewById(R.id.btnmycity);
        btnmyeducation1 = (Button) findViewById(R.id.btnmyeducation);
        btnmyfamily1 = (Button) findViewById(R.id.btnmyfamily);
        btnquiz1 = (Button) findViewById(R.id.btnquiz);
        btnquit1 = (Button) findViewById(R.id.btnquit);
        long lastPress;
        Toast backpressToast;

        //Button Profil Ketika DiKlik
        btnprofil1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getApplicationContext(), "Profil Telah Dipilih", Toast.LENGTH_SHORT).show();
                Intent beach = new Intent(MainActivity.this, MainActivity_Profil.class);
                startActivity(beach);
            }
        }
        );

        //Button MyCity Ketika DiKlik
        btnmycity1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            Toast.makeText(getApplicationContext(), "MyCity Telah Dipilih", Toast.LENGTH_SHORT).show();
            Intent beach = new Intent(MainActivity.this, MainActivity_MyCity.class);
            startActivity(beach);
            }
        }
        );

        //Button MyEducation Ketika DiKlik
        btnmyeducation1.setOnClickListener(new View.OnClickListener()
        {
             @Override
             public void onClick(View v)
             {
              Toast.makeText(getApplicationContext(), "MyEducation Telah Dipilih", Toast.LENGTH_SHORT).show();
              Intent beach = new Intent(MainActivity.this, MainActivity_MyEducation.class);
              startActivity(beach);
              }
        }
        );

        //Button MyFamily Ketika DiKlik
        btnmyfamily1.setOnClickListener(new View.OnClickListener()
        {
              @Override
              public void onClick(View v)
              {
               Toast.makeText(getApplicationContext(), "MyFamily Telah Dipilih", Toast.LENGTH_SHORT).show();
               Intent beach = new Intent(MainActivity.this, MainActivity_MyFamily.class);
               startActivity(beach);
              }
        }
        );

        //Button Quiz Ketika DiKlik
        btnquiz1.setOnClickListener(new View.OnClickListener()
         {
               @Override
               public void onClick(View v)
               {
                 Toast.makeText(getApplicationContext(), "Quiz Telah Dipilih", Toast.LENGTH_SHORT).show();
                 Intent beach = new Intent(MainActivity.this, MainActivity_Quiz.class);
                 startActivity(beach);
               }
         }
        );

        //Button Exit Ketika DiKlik
        btnquit1.setOnClickListener(new View.OnClickListener()
         {
                @Override
                public void onClick(View v)
                {
                    finish();
                    System.exit(0);
                 }
         }
        );


    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirm Exit")
                .setMessage("Are you sure you want to Close this Activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();

    }
}



